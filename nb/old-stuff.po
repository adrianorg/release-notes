# translation of old-stuff.po to Norwegian bokmål
# SOME DESCRIPTIVE TITLE
# Copyright (C) 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Klaus Ade Johnstad <klaus@skolelinux.no>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: old-stuff\n"
"POT-Creation-Date: 2021-03-27 22:34+0100\n"
"PO-Revision-Date: 2009-02-17 21:33+0100\n"
"Last-Translator: Klaus Ade Johnstad <klaus@skolelinux.no>\n"
"Language-Team: Norwegian bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "nb"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
#, fuzzy
#| msgid "Managing your &oldreleasename; system"
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Forberedelser for oppgradering av et &oldreleasename; system"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Dette tillegget inneholder informasjon om hvordan du kontrollerer at du "
"faktisk kan installere eller oppgradere pakker på &oldreleasename; "
"versjonen, før du oppgraderer til &releasename;. Dette er kun nødvendig i "
"helt spesifikke situasjoner."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Oppgradering av ditt &oldreleasename; system"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
#, fuzzy
#| msgid ""
#| "Basically this is no different than any other upgrade of &oldreleasename; "
#| "you've been doing.  The only difference is that you first need to make "
#| "sure your package list still contains references to &oldreleasename; as "
#| "explained in <xref linkend=\"old-sources\"/>."
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"Det er ingen større forskjeller i forhold til tidligere oppgraderinger du "
"har gjort på &oldreleasename;. Den eneste forskjellen er at du nå må passe "
"på at dine pakkearkiv refererer til &oldreleasename;, som beskrevet i <xref "
"linkend=\"old-sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Hvis du benytter deg av et Debian speil, så vil du atomatisk bli oppgradert "
"til den siste punktutgivelsen av &oldreleasename;."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
#, fuzzy
#| msgid "Checking your sources list"
msgid "Checking your APT source-list files"
msgstr "Kontroller dine arkivlister"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
#, fuzzy
#| msgid ""
#| "If any of the lines in your <filename>/etc/apt/sources.list</filename> "
#| "refer to 'stable', you are effectively already <quote>using</quote> "
#| "&releasename;.  If you have already run <literal>apt-get update</"
#| "literal>, you can still get back without problems following the procedure "
#| "below."
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""
"Hvis noen av linjene i <filename>/etc/apt/sources.list</filename> refererer "
"til 'stable', så bruker du i praksis <quote>allerede</quote> &releasename;. "
"Hvis du allerede har brukt <literal>apt-get update</literal> så kan du "
"fortsatt rette det opp igjen uten problemer om du følger oppskriften som "
"kommer her."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Om du allerede har begynt å installere pakker fra &releasename;, så er det "
"antakelig ingen grunn lenger  til å installere pakker fra &oldreleasename;. "
"Du må selv bestemme om du vil fortsette eller ikke. Det er mulig å "
"nedgradere pakker, men det er ikke beskrevet her."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
#, fuzzy
#| msgid ""
#| "Open the file <filename>/etc/apt/sources.list</filename> with your "
#| "favorite editor (as <literal>root</literal>) and check all lines "
#| "beginning with <literal>deb http:</literal> or <literal>deb ftp:</"
#| "literal> for a reference to <quote><literal>stable</literal></quote>.  If "
#| "you find any, change <literal>stable</literal> to "
#| "<literal>&oldreleasename;</literal>."
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Åpne fila <filename>/etc/apt/sources.list</filename> med din favoritt "
"tekstbehandler (som <literal>root</literal>) og kontroller alle linjer som "
"begynner med enten <literal>deb http:</literal> eller <literal>deb ftp:</"
"literal>, hvis noen av dem refererer til <quote><literal>stable</literal></"
"quote>, så bytt ut <literal>stable</literal> med <literal>&oldreleasename;</"
"literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
#, fuzzy
#| msgid ""
#| "If you have any lines starting with <literal>deb file:</literal>, you "
#| "will have to check for yourself if the location they refer to contains an "
#| "&oldreleasename; or a &releasename; archive."
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"Hvis du har noen linjer som begynner med <literal>deb file:</literal>, så må "
"du selv finne ut om det sted til referer til inneholder et arkiv for "
"&oldreleasename; eller &releasename; ."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
#, fuzzy
#| msgid ""
#| "Do not change any lines that begin with <literal>deb cdrom:</literal>.  "
#| "Doing so would invalidate the line and you would have to run <command>apt-"
#| "cdrom</command> again.  Do not be alarmed if a 'cdrom' source line refers "
#| "to <quote><literal>unstable</literal></quote>.  Although confusing, this "
#| "is normal."
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"Ikke endre noen linjer som begynner med <literal>deb cdrom:</literal>. Hvis "
"du gjør det, så blir det en feil på den linja, og du må kjøre kommandoen "
"<command>apt-cdrom</command> omigjen for å rette det opp. Bli ikke forvirret "
"om du ser en 'cdrom'-linje som referer til <quote><literal>unstable</"
"literal></quote>, der er ofte slik for CD-ROM."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr ""
"Om du har gjort noen endringer, så lagrer og avslutter du, og kjører "
"deretter:"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, fuzzy, no-wrap
#| msgid "# apt-get update\n"
msgid "# apt update\n"
msgstr "# apt-get update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "for å oppdatere pakkelisten"

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
