# German translation of the Debian release notes
#
# Helge Kreutzmann <debian@helgefjell.de>, 2009.
# Holger Wansing <linux@wansing-online.de>, 2010, 2011, 2013, 2015, 2017.
# Holger Wansing <hwansing@mailbox.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 9.0\n"
"POT-Creation-Date: 2021-03-27 22:34+0100\n"
"PO-Revision-Date: 2019-04-23 23:55+0100\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "de"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Verwalten Ihres &Oldreleasename;-Systems vor dem Upgrade"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Dieser Anhang enthält Informationen darüber, wie Sie sicherstellen, dass Sie "
"ein Upgrade von Paketen aus &Oldreleasename; durchführen oder diese "
"installieren können, bevor Sie das Upgrade auf &Releasename; durchführen. "
"Dies sollte nur in besonderen Situationen notwendig sein."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Upgrade Ihres &Oldreleasename;-Systems"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"Dem Grunde nach ist dies nichts anderes als jedes bisherige Upgrade von "
"&Oldreleasename;. Der einzige Unterschied besteht darin, dass Sie zuerst "
"sicherstellen müssen, dass Ihre Paketliste noch Referenzen für "
"&oldreleasename; enthält, wie es in <xref linkend=\"old-sources\"/> erklärt "
"ist."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Falls Sie zum Upgrade Ihres Systems einen Debian-Spiegel nutzen, so erfolgt "
"das Upgrade automatisch auf die neueste Zwischenveröffentlichung "
"(sogenanntes Point-Release) von &Oldreleasename;."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
msgid "Checking your APT source-list files"
msgstr "Überprüfen Ihrer Paketquellen (APT source-list-Dateien)"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""
"Falls sich Zeilen in Ihren APT source-list-Dateien (siehe <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>) auf "
"<quote>stable</quote> beziehen, zeigen sie effektiv schon auf &Releasename;-"
"Paketquellen. Dies ist möglicherweise nicht das, was Sie möchten, falls Sie "
"noch nicht bereit für das Upgrade sind. Wenn Sie bereits <command>apt "
"update</command> ausgeführt haben, können Sie ohne Probleme mit der unten "
"aufgeführten Anweisung wieder auf den alten Zustand zurückkehren."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Falls Sie bereits Pakete aus &Releasename; installiert haben, ergibt es "
"wahrscheinlich keinen Sinn mehr, Pakete aus &Oldreleasename; zu "
"installieren. In diesem Fall müssen Sie selbst entscheiden, ob Sie "
"fortfahren wollen oder nicht. Es besteht die Möglichkeit, zu alten "
"Paketversionen zurückzukehren, dies wird hier aber nicht beschrieben."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Öffnen Sie als <literal>root</literal> die entsprechende source-list-Datei "
"mit einem Editor und überprüfen Sie alle Zeilen, die mit <literal>deb http:</"
"literal>, <literal>deb https:</literal>, <literal>deb tor+http:</literal>, "
"<literal>deb tor+https:</literal>, <literal>URIs: http:</literal>, "
"<literal>URIs: https:</literal>, <literal>URIs: tor+http:</literal> oder "
"<literal>URIs: tor+https:</literal>beginnen, ob sie Referenzen auf "
"<quote><literal>stable</literal></quote> enthalten. Falls ja, ändern Sie "
"diese von <literal>stable</literal> in <literal>&oldreleasename;</literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"Falls Zeilen vorkommen, die mit <literal>deb file:</literal> oder "
"<literal>URIs: file:</literal> beginnen, müssen Sie selbst überprüfen, ob "
"der darin angegebene Ort ein Archiv von &Oldreleasename; oder &Releasename; "
"enthält."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"Ändern Sie keine Zeilen, die mit <literal>deb cdrom:</literal> oder "
"<literal>URIs: cdrom:</literal> beginnen. Dies würde dazu führen, dass die "
"Zeile ungültig wird und Sie <command>apt-cdrom</command> erneut ausführen "
"müssen. Es ist kein Problem, falls eine <quote>cdrom</quote>-Quellzeile "
"<quote><literal>unstable</literal></quote> enthält. Dies ist zwar "
"verwirrend, aber normal."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr ""
"Falls Sie Änderungen vorgenommen haben, speichern Sie die Datei und führen "
"Sie "

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, no-wrap
msgid "# apt update\n"
msgstr "# apt update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "aus, um die Paketliste neu einzulesen."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr "Veraltete Konfigurationsdateien entfernen"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
"Bevor Sie Ihr System auf &Releasename; aktualisieren, wird empfohlen, alte "
"Konfigurationsdateien (wie <filename>*.dpkg-{new,old}</filename>-Dateien in "
"<filename>/etc</filename>) vom System zu entfernen."

#~ msgid "Upgrade legacy locales to UTF-8"
#~ msgstr "Upgrade von veralteten Gebietsschemata (Locales) auf UTF-8"

#~ msgid ""
#~ "Using a legacy non-UTF-8 locale has been unsupported by desktops and "
#~ "other mainstream software projects for a long time. Such locales should "
#~ "be upgraded by running <command>dpkg-reconfigure locales</command> and "
#~ "selecting a UTF-8 default. You should also ensure that users are not "
#~ "overriding the default to use a legacy locale in their environment."
#~ msgstr ""
#~ "Die Verwendung einer nicht-UTF-8-Locale wird seit langer Zeit von Desktop-"
#~ "Umgebungen und anderen Mainstream-Software-Projekten nicht mehr "
#~ "unterstützt. Solche Locales sollten aktualisiert werden, indem Sie "
#~ "<command>dpkg-reconfigure locales</command> ausführen und dort eine UTF-8-"
#~ "Locale auswählen. Sie sollten auch sicherstellen, dass Benutzer die "
#~ "Standardeinstellung nicht überschreiben, um trotzdem eine alte Locale zu "
#~ "verwenden."

#~ msgid ""
#~ "Lines in sources.list starting with <quote>deb ftp:</quote> and pointing "
#~ "to debian.org addresses should be changed into <quote>deb http:</quote> "
#~ "lines."
#~ msgstr ""
#~ "Zeilen in sources.list, die mit <quote>deb ftp:</quote> beginnen und auf "
#~ "debian.org-Adressen verweisen, sollten in <quote>deb http:</quote>-Zeilen "
#~ "geändert werden."
